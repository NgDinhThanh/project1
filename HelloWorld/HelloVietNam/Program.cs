﻿// See https://aka.ms/new-console-template for more information
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
Console.InputEncoding = Encoding.UTF8;
Console.WriteLine("Nhập tên sinh viên: ");
string name = Console.ReadLine();
Console.WriteLine("Nhập lớp sinh viên: ");
string classRoom = Console.ReadLine();

Console.WriteLine($"Họ tên: {name}, lớp: {classRoom}.");
Console.ReadKey();